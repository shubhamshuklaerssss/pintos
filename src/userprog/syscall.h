#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include "threads/synch.h"
void syscall_init (void);
void exit_on_error(void);

#endif /* userprog/syscall.h */
